<h1>INSTALAÇÃO E DOCUMENTAÇÃO:</h1>

- https://nodejs.org/en/
- https://code.visualstudio.com/
- https://www.cypress.io/

<h1>CONSOLE:</h1>

"Executar como administrador";<br>
'npm install'.

Acessar a pasta onde esta o projeto;<br>
**Exemplo** - "cd AutomationAPI".<br>
**Exemplo** - "cd AutomationQA".

<h1>ABRIR O CYPRESS:</h1>

'npx cypress open'

<h1>RODAR O PROJETO:</h1>

Selecionar o arquivo 'RonaldKessler' dentro do cypress quando executar o projeto AutomationAPI.

<b>E</b>

Selecionar o arquivo 'RealizarCompra' ou 'ValidarDados' dentro do cypress quando executar o projeto AutomationQA.
